
<script>
 //display a confirmation box when trying to delete an object
 
 function showConfirm(id)
 {
     //build the confirmation box
     
     var c = confirm("Are you sure you want to delete the item?");
     //if true, delete item and refresh
     if(c)
         window.location = 'CoffeeOverview.php?delete='+id;
     
 }
 
</script>




<?php

require ("Model/CoffeeModel.php");

require ("Model/Credentials.php");#for testing 


/**
 * Description of CoffeeController
 * contains non-database related for the coffee webpage
 *
 * @author lsrs
 */

class CoffeeController {
    function CreateOverviewTable() {
       $coffeeModel = new CoffeeModel();//object containing the corresponding information of each class of coffee product
        $result = "<table class='overviewTable' border='1' width='1' cellspacing='1' cellpadding='1'>
        <thead>
            <tr>
                <th>Update</th>
                <th>Delete</th>
                <th>Id</th>
                <th>Name</th>
                <th>Type</th>
                <th>Price</th>
                <th>Roast</th>
                <th>Country</th>
            </tr>
        </thead>
        <tbody>
            ";
        $types = $coffeeModel->GetCoffeeTypes();//here I get all types of coffee
        //iterate over the array of types    
         foreach ($types as $type) 
        {
        $coffeeArray = $coffeeModel -> GetCoffeeByType($type);  
             foreach ($coffeeArray as $key => $value) 
                {
                    $result = $result . "<tr>
                        <td><a href = 'CoffeeAdd.php?update=$value->id'>Update</a></td>
                        <td><a href = '#' onclick='showConfirm($value->id)'>Delete</a></td>
                        <td>$value->id</td>
                        <td>$value->name</td>
                        <td>$value->type</td>
                        <td>$value->price</td>
                        <td>$value->roast</td>
                        <td>$value->country</td>
                    </tr>";
                }//end for each value in coffearray
        }//end for each type
        $result = $result . "</tbody> </table>";
        return  $result;
    }//end of OverviewTable
   
    function CreateCoffeeDropdownList() {
        $coffeeModel = new CoffeeModel();
        $result = "<form action = '' method='post' width = '200px'>
                    Please select a type: 
                    <select name='Subject'>
                    <option value = '%' >All</option>
                    ".$this-> CreateOptionValues($coffeeModel->GetCoffeeTypes()).
                    "</select>
                    <input type='submit' name='submit' value='Submit Option!'>
                    </form>";
                        
        return $result;
        
    }
    
    function CreateOptionValues(array $valueArray) {
        $result = "";
        foreach ($valueArray as $value) 
        {
            $result = $result . "<option value = '$value'>$value</option>";
        }//end for each
        return $result;
    }//end of function CreateOptionValues
   
    function CreateCoffeeTables($type) {
        $coffeeModel = new CoffeeModel();//object containing the corresponding information of each class of coffee product
        $coffeeArray = $coffeeModel -> GetCoffeeByType($type);                   
        $result = "";
        foreach ($coffeeArray as $key => $coffee) {
            $result = $result . 
                    "<table class = 'coffeeTable'>
                        <tr>
                            <th rowspan = '6' width = '150px' ><img runat = 'server' src= '$coffee->image' /> </th>
                            <th width = '75px' >Name:</th>
                            <td>$coffee->name</td>
                        </tr>
                         <tr>
                            <th>type:</th>
                            <td>$coffee->type</td>
                        </tr>
                         <tr>
                            <th>Price</th>
                            <td>$coffee->price</td>
                        </tr>
                         <tr>
                            <th>Roast</th>
                            <td>$coffee->roast</td>
                        </tr>
                         <tr>
                            <th>Origin</th>
                            <td>$coffee->country</td>
                        </tr>
                        <tr>
                            <td colspan='2' >$coffee->reviews</td>
                        </tr>
                    </table>";
        }//end for each

        return $result;
    }//
    
    //<editor-fold desc="Set Methods">

    function InsertCoffee(){
        
        $name = $_POST["txtName"];
        $type = $_POST['ddlType'];
        $price = $_POST['txtPrice'];
        $roast = $_POST['txtRoast'];
        $country = $_POST['txtCountry'];
        $image = $_POST['ddlImage'];
        $reviews = $_POST['textReviews'];
        $coffee = new CoffeeEntity(-1, $name, $type, $price, $roast, $country, $image, $reviews);
        $coffeeModel = new CoffeeModel();
        $coffeeModel ->InsertCoffee($coffee);
        
       
    }//end of function InsertCoffee()

    function UpdateCoffee($id){
        $name = $_POST["txtName"];
        echo  $name . ' in coffee controller';
        $type = $_POST['ddlType'];
        
        echo $type;
        
        $price = $_POST['txtPrice'];
        $roast = $_POST['txtRoast'];
        $country = $_POST['txtCountry'];
        $image = $_POST['ddlImage'];
        $reviews = $_POST['textReviews'];
        
        $coffee = new CoffeeEntity($id, $name, $type, $price, $roast, $country, $image, $reviews);
        
        $coffeeModel = new CoffeeModel();

        $coffeeModel->UpdateCoffee($id, $coffee);
        
    }
    
    function DeleteCoffee($id){
        
         $coffeeModel = new CoffeeModel();

        $coffeeModel->DeletCoffee($id);
        
    }

    //</editor-fold>

    //<editor-fold desc="Get Methods">
    
    
    function GetImages(){
        //select folder to scan
        $handle = opendir("Images/Coffee");
        //read all files and store names in array
        $image1 = [];
        while ($image = readdir($handle)) {
             array_push($image1, $image);
            #$image[] = $image;
        }//end while
        closedir($handle);
        //exclude all file name, where filename < 3
        $imageArray = array();
        foreach ($image1 as $image) {
            if ((strlen($image))>2) {
                array_push($imageArray, $image);
            }//endif strlen
        }//end for each
        //create <select><option> values and return result                
                //echo $imageArray;
        $result = $this->CreateOptionValues($imageArray);
        return $result;
    }//end of function GetImages()
    
    
    
    
     function GetCoffeeById($id){
         $coffeeModel = new CoffeeModel();
         return $coffeeModel->GetCoffeeById($id);
    }

    function GetCoffeeByType($type){
        $coffeeModel = new CoffeeModel();
         return $coffeeModel->GetCoffeeByType($type);
        
    }

    function GetCoffeeTypes(){
        $coffeeModel = new CoffeeModel();
         return $coffeeModel->GetCoffeeTypes();
    }
    
    //</editor-fold>

}//end of class

  ?>


    












































