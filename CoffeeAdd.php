<?php

require './Controller/CoffeeController.php';

//require ("Entities/CoffeeEntity.php")
$coffeeController = new CoffeeController();
$coffeeModel = new CoffeeModel();
$title ='Add a New Coffee';

if (isset($_GET["update"])) {
        
    $coffeeArray = array();
    $coffeeArray = $coffeeController->GetCoffeeById($_GET["update"]);
   // $coffee = new CoffeeEntity($id, $name, $type, $price, $roast, $country, $image, $reviews);

    $coffee =$coffeeArray[0];    
    //echo $coffee;
    $content =   "<form action='' method='POST'>
    <fieldset>
        <legend>Add a New Coffee</legend>
        <label for='name'>Name: </label>
        <input type='text' class='inputField' name='txtName' value='$coffee->name'/><br/>
        
        <label for='type'>Type: </label>
        <select class = 'inputField' name='ddlType'> 
            <option value='%'>All</option>"
        . $coffeeController-> CreateOptionValues($coffeeModel->GetCoffeeTypes()).
        "</select><br/>
        <label for='price'>Price: </label>
        <input type='text' class='inputField' name='txtPrice' value='$coffee->price'/><br/>
        
        <label for='roast'>Roast: </label>
        <input type='text' class='inputField' name='txtRoast' value='$coffee->roast'/><br/>
        
        <label for='country'>Country: </label>
        <input type='text' class='inputField' name='txtCountry' value='$coffee->country'/><br/>
        
        <label for='image'>Image: </label>
        <select class='inputField' name='ddlImage'>"
         .$coffeeController->GetImages().
        "</select><br/>
        
        <label for='reviews'>Reviews: </label>
        <textarea cols='70' rows='12' name='textReviews'>'$coffee->reviews'</textarea><br/>
        <input type='submit' value='Submit'>
    </fieldset>
</form>";
    
}

 else {
     
      $content =   "<form action='' method='POST'>
    <fieldset>
        <legend>Add a New Coffee</legend>
        <label for='name'>Name: </label>
        <input type='text' class='inputField' name='txtName'/><br/>
        
        <label for='type'>Type: </label>
        <select class = 'inputField' name='ddlType'> 
            <option value='%'>All</option>"
        . $coffeeController-> CreateOptionValues($coffeeModel->GetCoffeeTypes()).
        "</select><br/>
        <label for='price'>Price: </label>
        <input type='text' class='inputField' name='txtPrice'/><br/>
        
        <label for='roast'>Roast: </label>
        <input type='text' class='inputField' name='txtRoast'/><br/>
        
        <label for='country'>Country: </label>
        <input type='text' class='inputField' name='txtCountry'/><br/>
        
        <label for='image'>Image: </label>
        <select class='inputField' name='ddlImage'>"
         .$coffeeController->GetImages().
        "</select><br/>
        
        <label for='reviews'>Reviews: </label>
        <textarea cols='70' rows='12' name='textReviews'></textarea><br/>
        
        <input type='submit' value='Submit'>
    </fieldset>
</form>";
    
}

if (isset($_GET['update'])) {
   if (isset($_POST['txtName'])) {
    $coffeeController->UpdateCoffee(($_GET['update']));
    }//endif
}//endif
else{    
   if (isset($_POST['txtName'])) {
    $coffeeController->InsertCoffee();
    } 
}











$sidecontent =  '<input type="submit" value="Logout!" name="Logout" />';

include 'Template.php';

?>


         