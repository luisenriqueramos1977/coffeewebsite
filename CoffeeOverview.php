<?php

require './Controller/CoffeeController.php';

$title ='Manage Product';

$coffeeController = new CoffeeController();

$content =   $coffeeController->CreateOverviewTable();

$sidecontent =  '<input type="submit" value="Logout!" name="Logout" />';
 
//prepare to delete


if ((isset($_GET["delete"]))!=null)
{
    $coffeeController->DeleteCoffee($_GET["delete"]);
    
}


include './Template.php';
  
 ?>
