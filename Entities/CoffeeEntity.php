<?php


class CoffeeEntity {
    //columns in data base
    
    public $id;
    public $name;
    public $type;
    public $price;
    public $roast;
    public $country;
    public $image;
    public $reviews;

    function __construct($id, $name, $type, $price, $roast, $country, $image, $reviews) {
        $this->id = $id;
        $this->name = $name;
        $this->type = $type;
        $this->price = $price;
        $this->roast = $roast;
        $this->country = $country;
        $this->image = $image;
        $this->reviews = $reviews;
    }

    
    
    
    
}

?>