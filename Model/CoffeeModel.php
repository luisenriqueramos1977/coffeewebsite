<?php

require ("Entities/CoffeeEntity.php");

    //cointains database related code for the coffee page


class CoffeeModel {
    
    //get all coffee types from the database and return them in an array

    function GetCoffeeTypes() {
        require 'Credentials.php';
        $con = mysqli_connect($host, $user, $passwd, $database);
        if (mysqli_connect_errno()) {
            #code
            echo 'fail to connect to my sql' .mysqli_connect_error();
        }
        mysqli_select_db($con, $database);
        $result = mysqli_query($con, "SELECT DISTINCT type FROM coffee");
        $types = array();
        //get data from database
        while ($row = mysqli_fetch_array($result)) 
        {
            array_push($types, $row[0]);
        }//end while
        //close connection and return results
        mysqli_close($con);
        return $types;
        
    }//end of function function getCoffeeTypes
    
    //get coffee entities objects and return them in an array 
    
    function GetCoffeeByType($type) {
        require 'Credentials.php';
        $con = mysqli_connect($host, $user, $passwd, $database);
        if (mysqli_connect_errno()) {
            #code
            echo 'fail to connect to my sql' .mysqli_connect_error();
        }
        mysqli_select_db($con, $database);
        $result = mysqli_query($con, "SELECT * FROM coffee WHERE type LIKE '$type'");
        $coffeeArray = array();
        //get data from database
         while ($row = mysqli_fetch_array($result)) 
        {
        $id= $row[0];
        $name = $row[1];
        $type= $row[2];
        $price= $row[3];
        $roast= $row[4];
        $country= $row[5];
        $image= $row[6];
        $reviews= $row[7];
        //create coffee objects and store them in an array
        $coffee = new CoffeeEntity($id, $name, $type, $price, $roast, $country, $image, $reviews);
        array_push($coffeeArray, $coffee);
        }//end while
        //close connection and return result 
          mysqli_close($con);
        return $coffeeArray;
        
    }
    
    //get coffee by id 
    
    function GetCoffeeById($id) {//check with this-> to verify return
        require 'Credentials.php';
        $query = "";
        $con = mysqli_connect($host, $user, $passwd, $database);
        if (mysqli_connect_errno()) {
            #code
            echo 'fail to connect to my sql' .mysqli_connect_error();
        }
        mysqli_select_db($con, $database);
        $result = mysqli_query($con, "SELECT * FROM coffee where id = '$id'");
        $coffeeArray = array();
        //get data from database
         while ($row = mysqli_fetch_array($result)) 
        {
        $id= $row[0];
        $name = $row[1];
        $type= $row[2];
        $price= $row[3];
        $roast= $row[4];
        $country= $row[5];
        $image= $row[6];
        $reviews= $row[7];
        //create coffee objects and store them in an array
        $coffee = new CoffeeEntity($id, $name, $type, $price, $roast, $country, $image, $reviews);
        array_push($coffeeArray, $coffee);
        }//end while
        //close connection and return result 
          mysqli_close($con);
        return $coffeeArray;
        
    }//end of function function GetCoffeeById
    
    //function for insert one coffee object
    
    function InsertCoffee(CoffeeEntity $coffee) {
         require 'Credentials.php';
         $con = mysqli_connect($host, $user, $passwd, $database);
         $query = "";
         $query = sprintf("INSERT INTO coffee
                 (name, type, price, roast, country, image, review)
                 VALUES
                 ('%s','%s','%s','%s','%s','%s','%s')",
         mysqli_real_escape_string($con, $coffee->name),
         mysqli_real_escape_string($con, $coffee->type),
         mysqli_real_escape_string($con, $coffee->price),
         mysqli_real_escape_string($con, $coffee->roast),
         mysqli_real_escape_string($con, $coffee->country),
         mysqli_real_escape_string($con, "Images/Coffee/".$coffee->image),
         mysqli_real_escape_string($con, $coffee->reviews));
         
        $this->PerformQuery($query);
       
    }//end of function function GetCoffeeById
    
    //function for update a coffe in the list
    
    function UpdateCoffee($id, CoffeeEntity $coffee) {
        echo $coffee->name . ' in function  UpdateCoffee';
        
        require 'Credentials.php';
        
         $con = mysqli_connect($host, $user, $passwd, $database);
         
         if(! $con ) {
        die('Could not connect: ' . mysqli_error());
        }
         
         $query = "";
         $query = sprintf("UPDATE coffee
                                SET name = '%s', type = '%s', price= '%s', roast= '%s', country= '%s', image= '%s', reviews= '%s'
                                where id = '$id'",
         mysqli_real_escape_string($con, $coffee->name),
         mysqli_real_escape_string($con, $coffee->type),
         mysqli_real_escape_string($con, $coffee->price),
         mysqli_real_escape_string($con, $coffee->roast),
         mysqli_real_escape_string($con, $coffee->country),
         mysqli_real_escape_string($con, "Images/Coffee/".$coffee->image),
         mysqli_real_escape_string($con, $coffee->reviews));
                                    
         if (mysqli_query($con, $query)) {
            echo "Record updated successfully";
        } else {
            echo "Error updating record: " . mysqli_error($con);
        }
        mysqli_close($con);
        //$this->PerformQuery($query);
        
    }//end of function function GetCoffeeById
    
    //function for delete coffee
    
    function DeletCoffee($id) {
       
        $query = "";
        
        $query = "DELETE FROM coffee where id = '$id'";

        $this->PerformQuery($query);

    }//end of function function GetCoffeeById
    
    //general function for performing queries to the database
    
    function PerformQuery($query) {
         require 'Credentials.php';
        $con = mysqli_connect($host, $user, $passwd, $database);
        if (mysqli_connect_errno()) {
            #code
            echo 'fail to connect to my sql' .mysqli_connect_error();
        }
        mysqli_select_db($con, $database);
        $result = mysqli_query($con, $query);
        mysqli_close($con);
        return $result;
    }//end of function function GetCoffeeById
    
}//end of coffee model

?>